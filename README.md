## [](#static-website-example-cicd)static-website-example-cicd.

Continuous CI/CD deployment of a static web application on Gitlab.


### Prerequisites
 - Install docker 
 - install git
 - Have an account on the cloud provider Heroku

### Start
1. From Heroku get your token that will allow us to authenticate from outside:
**Account Settings -> API Key**
2. From Gitlab to authenticate on Heroku we have to set the environment variable HEROKU_API_KEY:
**static-website-example-cicd -> Setting -> CI/CD -> Variables -> Expand -> Add variable** 

![](https://lh6.googleusercontent.com/EbW5lQJHOGbwzdHI5cKgemg0swoppeEaqVqyJd-0X9eJp4i53YTrGwPONJLg0pNBI4m5ewDBjzlgG1yUHiRh-apFKzQJj4X8qkuhrb63lCTvyHfQf2FbuNcIwLgU3zpatUzNsxrtr6e4sL6vlR8myeD_zUTrE7QLOSZsHlQRiTr1wfNy3b65jefgGg)

3. Define the name of our image in the variable IMAGE_NAME:

![](https://lh5.googleusercontent.com/rypXs5bZEYvJRL9JInmeZu6FLui4SjhwT3js7pbnpRa0GFYymZeo0GXIiJHNJUyBJjb7u1T5XD54w4b2ifLfmHTyo2LOWJ47lzNX7b1QeuH4pnuhJMXXQV0CRLjWvgNkqJn_Q7t9g640-drCENc1ASO-v3J5cEibNoY2QpI8QyPqADpfsywI3hMwyg)

4. According to the .gitlab-ci.yml file:

- Deployment is only done on the master branch

After an update of the readme and commit on the main branch, the pipeline only executes on 4 steps because we are not on the master branch:

![](https://lh3.googleusercontent.com/MA8ge6m_DfU6fIo3_sIn39mrmpI1jR382VBNFIQjQt_8crmm_OwecA58QWAm5KGkDND3m-pKj_X0LLry5DPtuVqL9aeSaExSvSOJ0sc1pxZ0c8YuchzL7lunHKhnLx-KUc0r-nfrMXc8smO1EG_0ZypFGLirmYWOIBvQoWeJxrbpFgKgReV5t6nT4Q)

- The deployment of the Revue environment is only done during the merge request.

![](https://lh3.googleusercontent.com/UDtkbIzKXZ9B1hMRZ7kpuae6YI0xmgYBSlnpxNHK8dpfFZI-QAwvniSJ860skOhbqf6lkrNWF6r4e7_hTclZt469iJRPjRuCUuKHLkaWvJwco5P0YF8lWIqbeDph2tq5KHShl4NpVtqhSdmUqYxCmNEaT5NInOrVFGBeyAsMiNRT-XlTWcsMQs4AHw)

- stop review is done manually

![](https://lh4.googleusercontent.com/v7EOK8L-S3XnBSiasCOoMy0F59kvje-oYj8atots31eDWDtl-Ro5jAJch5qkGNsJo6Dv6vyiJL_V8HEmaYjlkM-1UBwJr8OZxmEY75Pz27OX92mJOR9fQm69CkjgLIyw1Lty8KdbSkiUONFR9YeXsEz1U7JeO_zqmHk9fWyMqsnWN9bVu8mWpDsuvQ)
![](https://lh6.googleusercontent.com/_GXTvERRi-5Vy5GM9TEVIv57frXb4b8ODH03X92inLMj4dHHvS4lxRCMVBS7J9oj9dCXEbdQzNOmDa7SimQpia31czxyBYB9l4B0j7ZYfAaUouph1UIfv1HjZvVV_Yzy47Yn0L-lVRedmH9be6eoGX2ojxYcJiRODHynvhOxQqUX4o0baq0xlyFlJg)

After merging the main and master branches the complete pipeline runs
![](https://lh6.googleusercontent.com/lupBAxbuxc3ZlrNT84WkEzLuGtWRGw_MjyWmQbajbYbUooza971bRbIA0stmbV51VU-HU3wWp15ovqO_gyTeK6kLuJNXOzHujDdAA9I3xZC1Vduvh4TWea3dr2gytkwJ6Jzk3bq2euVZLvHV1HDtP5azehXXaq3MtTI-Kqlw9pg0r_oZ0s38s9yLuQ)
![](https://lh6.googleusercontent.com/XInSn7xiTMqkrp-li9oCqNUvhnFz9fwfBerHf8MaSwOtIV42MO4cuqSFOqV4WEVboNBkJQFmjHkOiK0vyAkwzptO71Hgqno96Kl2MhMDRnjaisEqBASmCx8a0BfX9mqKPBx6GsA-6sOgFAL2BB9jFRoG2cBegVi8zPz5p-ZgQS2xEp3Plas2ZxcEYQ)
5. Test

Go to: Deployments -> Environments

![](https://lh3.googleusercontent.com/8cycUpLkbAP2GACl3utMthN1d8DTPbUPEepHqaj4MIhtdt2EPsnGBCipApyZG14vwXuYSIaSLn1yKMWEiASCzakoRzXa08RbUSYNmLfCj5uK2VkEPgGm44u45DBP8pqml7n8Y0kBj6iAURE7TZOR3ZmkRIta-T7cqHMZvPODgv7suYuq6pkHz47A3w)
Open staging environment:

![](https://lh3.googleusercontent.com/ZQlWnyOIjUjfDgkvdIYY1uMfKXES2cG44Rbfh1xOFAg1hF4UqE6mEboMXEX4-Ei2kqDslFnFY6duTuxLIULliKd9nN_YE_NOrdADI9e9radqKwjHWFjzuRXRsVt2ZW0Zqz9GsPB0X2BiG-nYDf3VFiC-dpCQBf2ekzvBk02i1zKqkEz_9mQcW1-rKg)

Open Production environment:

![](https://lh6.googleusercontent.com/BfsS0bVqu4-8sOmRpZa7ts-TXYQC0U25Xf6Uaf5QOetUOf1--IJ-q4fek5fsUQpzwYoAayeKINOcMmUi1m8POcafJjaaHsjpOU9OrEHiRBrco5sk6aW3YyJr2bGQZCaxMq5KkiO2wyycwj5xEwq2S5mRw2MOF8_EwAOuGCVc21O5cuWHZTkaobwDVQ)
